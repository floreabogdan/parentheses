﻿using Moq;
using NUnit.Framework;
using Parentheses.Business.Contracts.Repositories;
using Parentheses.Business.Services;

namespace Parentheses.Business.Tests.Services
{
    [TestFixture]
    public class ParenthesesServiceTests
    {
        private ParenthesesService _service;
        private Mock<IInputRepository> _inputReader;

        [SetUp]
        public void SetUp()
        {
            _inputReader = new Mock<IInputRepository>();
            _service = new ParenthesesService(_inputReader.Object);
        }

        [Test]
        public void Validate_InputIsEmptyOrNull_ThrowArgumentNullException()
        {
            _inputReader.Setup(ir => ir.GetInput()).Returns("");

            Assert.That(() => _service.Validate(), Throws.ArgumentNullException);
        }

        [Test]
        [TestCase("[( I Like this )]")]
        public void Validate_InputIsValid_ReturnValidResponse(string input)
        {
            _inputReader.Setup(ir => ir.GetInput()).Returns(input);

            var result = _service.Validate();

            Assert.That(result.IsValid, Is.True);
            Assert.That(string.IsNullOrWhiteSpace(result.Error), Is.True);
        }

        [Test]
        [TestCase("this ( is [ a ) text ] where the parentheses are ) incorrect.", " a ) te")]
        [TestCase("[(]", "[(]")]
        [TestCase("(", "(")]
        [TestCase(")", ")")]
        [TestCase("it is ()[", " ()[")]
        [TestCase("[this is a test", "[thi")]
        public void Validate_InputIsInvalid_ReturnInvalidResponse(string input, string expectedResult)
        {
            _inputReader.Setup(ir => ir.GetInput())
                .Returns(input);

            var result = _service.Validate();

            Assert.That(result.IsValid, Is.False);
            Assert.That(string.IsNullOrWhiteSpace(result.Error), Is.False);
            Assert.That(result.Error, Is.EqualTo(expectedResult));
        }
    }
}
