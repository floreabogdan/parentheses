﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Parentheses.Business.Contracts.Repositories;
using Parentheses.Repositories;

namespace Parentheses.Factories
{
    public class InputFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public InputFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IInputRepository GetRepository(string name)
        {
            switch (name)
            {
                case "console":
                    return _serviceProvider.GetRequiredService<ConsoleRepository>();

                case "hardcoded":
                    return _serviceProvider.GetRequiredService<HardCodedRepository>();

                default:
                    throw new ArgumentException("Provided argument is invalid!");
            }
        }
    }
}
