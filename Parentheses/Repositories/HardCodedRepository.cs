﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parentheses.Business.Contracts.Repositories;

namespace Parentheses.Repositories
{
    class HardCodedRepository : IInputRepository
    {
        public string GetInput()
        {
            string toTest = "( demo text ( [test] ( just a small ( text ( with all kind of { parentheses in {( different (locations ) who ) then } also } with ) each other ) should ) match ) or ) not... )";

            Console.WriteLine($"The following string will be tested: \n{toTest}");

            return toTest;
        }
    }
}
