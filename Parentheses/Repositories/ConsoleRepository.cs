﻿using System;
using Parentheses.Business.Contracts.Repositories;

namespace Parentheses.Repositories
{
    public class ConsoleRepository : IInputRepository
    {
        public string GetInput()
        {
            Console.Write("Please provide your string: ");
            return Console.ReadLine();
        }
    }
}
