﻿using System;
using Microsoft.Extensions.Logging;
using Parentheses.Business.Contracts.Repositories;
using Parentheses.Business.Contracts.Services;
using Parentheses.Factories;

namespace Parentheses
{
    public class Application
    {
        private readonly ILogger<Application> _logger;
        private readonly IParenthesesService _parenthesesService;
        private readonly InputFactory _inputFactory;

        public Application(ILogger<Application> logger, IParenthesesService parenthesesService, InputFactory inputFactory)
        {
            _logger = logger;
            _parenthesesService = parenthesesService;
            _inputFactory = inputFactory;
        }

        public void Run(string[] args)
        {
            try
            {
                bool shouldExit = false;
                IInputRepository inputMethod = _inputFactory.GetRepository("console");

                do
                {
                    Console.WriteLine("Please select an option: ");
                    Console.WriteLine("1. Use console input");
                    Console.WriteLine("2. Use hardcoded input");
                    Console.Write("Your option: ");

                    string option = Console.ReadLine();

                    switch (option)
                    {
                        case "1":
                            inputMethod = _inputFactory.GetRepository("console");
                            shouldExit = true;
                            break;

                        case "2":
                            inputMethod = _inputFactory.GetRepository("hardcoded");
                            shouldExit = true;
                            break;
                    }
                } while (!shouldExit);

                _parenthesesService.SetInputMethod(inputMethod);

                var result = _parenthesesService.Validate();

                if (result.IsValid)
                {
                    Console.WriteLine("Your string is valid!");
                }
                else
                {
                    Console.WriteLine($"Your string is not valid. Please check your input near \"{result.Error}\"");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occured while processing your string!");
            }

            Console.ReadLine();
        }
    }
}
