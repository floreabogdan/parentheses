﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Parentheses.Business.Config;
using Parentheses.Business.Contracts.Repositories;
using Parentheses.Factories;
using Parentheses.Repositories;

namespace Parentheses
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();

            ConfigureServices(serviceCollection);

            serviceCollection.BuildServiceProvider()
                .GetService<Application>().Run(args);
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddLogging(opt =>
            {
                opt.AddConsole();
            });

            serviceCollection.AddTransient<Application>();
            serviceCollection.AddTransient<IInputRepository, HardCodedRepository>();

            serviceCollection.AddScoped<ConsoleRepository>();
            serviceCollection.AddScoped<HardCodedRepository>();

            serviceCollection.AddScoped<InputFactory>();

            // register business layer.
            serviceCollection.RegisterBusiness();
        }
    }
}
