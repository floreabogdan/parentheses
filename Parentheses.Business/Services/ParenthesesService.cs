﻿using System;
using System.Collections.Generic;
using System.Linq;
using Parentheses.Business.Contracts.Repositories;
using Parentheses.Business.Contracts.Services;
using Parentheses.Business.Domain;

namespace Parentheses.Business.Services
{
    public class ParenthesesService : IParenthesesService
    {
        private Dictionary<char, char> _validationPairs;
        private IInputRepository _inputRepository;

        public ParenthesesService(IInputRepository inputRepository)
        {
            _inputRepository = inputRepository;
            _validationPairs = new Dictionary<char, char>()
            {
                { '(', ')' },
                { '[', ']' },
                { '{', '}' }
            };
        }

        public void SetDictionary(Dictionary<char, char> dictionary)
        {
            _validationPairs = dictionary;
        }

        public void SetInputMethod(IInputRepository inputRepository)
        {
            _inputRepository = inputRepository;
        }

        public ValidationResult Validate()
        {
            Stack<char> parentheses = new Stack<char>();
            int lastKnownPosition   = 0;

            string input = _inputRepository.GetInput();

            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentNullException();
            }

            try
            {
                int iteration = 0;
                foreach (char c in input)
                {
                    // check if the char is
                    if (_validationPairs.ContainsKey(c))
                    {
                        lastKnownPosition = iteration;
                        parentheses.Push(c);
                    }
                    else
                    {
                        if (_validationPairs.ContainsValue(c))
                        {
                            lastKnownPosition = iteration;
                            if (c == _validationPairs[parentheses.Peek()])
                            {
                                parentheses.Pop();
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    iteration++;
                }
            }
            catch(InvalidOperationException)
            {
                // this is the case when we found a closing character
                // before any opening character.

                return new ValidationResult
                {
                    IsValid = false,
                    Error   = _getValidationMessage(input, lastKnownPosition)
                };
            }

            // if the stack is empty then we don't have a case for mismatched parentheses
            if(parentheses.Count() == 0)
                return new ValidationResult { IsValid = true };

            // otherwise we return a message containing the last position of a mismatched character.
            return new ValidationResult
            {
                IsValid = false,
                Error   = _getValidationMessage(input, lastKnownPosition)
            };
        }

        private string _getValidationMessage(string input, int position)
        {
            int startPosition = Math.Max(position - 3, 0);
            int length = Math.Min(input.Length - startPosition, position - startPosition + 4);

            var result = input.Substring(startPosition, length);

            return result;
        }
    }
}
