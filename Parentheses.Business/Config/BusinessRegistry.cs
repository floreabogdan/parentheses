﻿using Microsoft.Extensions.DependencyInjection;
using Parentheses.Business.Contracts.Services;
using Parentheses.Business.Services;

namespace Parentheses.Business.Config
{
    public static class BusinessRegistry
    {
        public static IServiceCollection RegisterBusiness(this IServiceCollection services)
        {
            services.AddTransient<IParenthesesService, ParenthesesService>();

            return services;
        }
    }
}
