﻿namespace Parentheses.Business.Domain
{
    public class ValidationResult
    {
        public bool IsValid { get; set; }

        public string Error { get; set; }
    }
}
