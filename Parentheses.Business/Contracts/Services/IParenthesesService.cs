﻿using System.Collections.Generic;
using Parentheses.Business.Contracts.Repositories;
using Parentheses.Business.Domain;

namespace Parentheses.Business.Contracts.Services
{
    public interface IParenthesesService
    {
        void SetInputMethod(IInputRepository inputRepository);

        void SetDictionary(Dictionary<char, char> dictionary);

        ValidationResult Validate();
    }
}
