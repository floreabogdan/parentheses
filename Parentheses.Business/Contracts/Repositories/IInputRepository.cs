﻿namespace Parentheses.Business.Contracts.Repositories
{
    public interface IInputRepository
    {
        string GetInput();
    }
}
